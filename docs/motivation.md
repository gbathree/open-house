## Two Trees

Our technological/cultural history is like a tree.  Design and 'normative' decisions, like branching wood, have led us down a path and have set the framework for how we live, how we spend our time, what we think is important, and where we put our energy.  In humans first 100,000 years, we made only a few small but important branches - fire, art, religion...  But in the last 1000 years (and especially last 50), we've accelerated that branching so fast we can't test ideas, prune bad paths, or even see back far enough to ask how and why we're doing what we're doing.

Tthe evolutionary history of our physical bodies (including our brain) is also like a tree with branches.  Those branches grow and fork slowly, over millions of years, in a highly coordinated and integrated way.  We can't really change those branches and can barely understand it and (let's be honest) if we do try to change it we're likely to screw it up.

Like most things in the universe, there are no inherent 'wrong' branches.  Suns grow and shrink, planets fly in orbits, spiders eat flies, dogs sniff butts.  Those are paths, and who am I to judge them.

But humans, unlock rocks, are opinionated - we (as a species at least) want a path which is long lasting and involves some je ne sais quoi... happiness? fulfillment? meaning? connection? elegance?  Not sure exactly, but it's somewhere in there.

I want that.  Or at least I want the ability to think about wanting it.  You too?  Cool.

And herein lies the problem: our technological/cultural paths are beginning to strain the slower paths taken by our bodies and minds.  It's as if we're a person walking down the road and their skin suddently turned left and their bones kept going straight.  Even if we don't tear ourselves apart, it's going to be painful and unpleasant.

I'm increasingly feeling this unpleasantness.  And like lobsters in a pot, I can't quite tell if it's 80 degrees or 150 degrees because I've been in here my whole life.  You too?  Ok then.

So let's cut through this metaphor and hand waiving for a minute: what am I talking about here precisely?  I'm talking about how we sit all day and wonder why we have hemmroids and back problems.  I'm talking about how many people spend large portions of their life doing things they don't care about which breeds depression, anxiety, and feeling disconnected.  I'm talking about the unending shitstream of stuff we produce and consume and throw away which don't contribute to our je ne sais quoi just to keep the wheel spinning.  I'm talking about the energy we put into services and people and stuff that feeds our worst instincts.

So... in short... all the things.

## Too many problems

Lost of little problems are time consuming but are solvable one by one.  Back problems - standing desk!  Depression - yoga!  Hemmroids - witch hazel!  But thats kind of what got us into this mess - looking back one branch and saying (like the smart little kids that we are) 'hey I can fix that!' and then giving ourselves a gold star.

We need to step back and look at root causes.

> ...(logical interlude) for those inclined, feel free to engage in the logic of one of the following arguments if that further motivates you.  Don't worry, if this all sounds like a bunch of bullshit, you can skip it and keep reading.

> 1. Our modern capitalist system must be replaced by...
> 2. Only through a personal connection with -->fill in diety name here<-- can we...
> 3. The oligarchy wants to prevent the flourishing of individuals through distraction and confusion...

Actually, the root cause is way more mundane... it's a big complex problem which is hard to build a business model around and very few individuals are super interested in it.  It's easier and feels better to solve little problems... so we do.

## Open House

The goal of this little project is to take on the big problem.  Big problems get solved by creating a framing which helps guide decisions, put little problems into buckets and help us keep the core issue in mind, even when we're down in the weeds.  So this repo is split into **Design Buckets** which represent the organization of the actual work and designs and **Design Concepts** which are cross-cutting principles we need to apply to all work done (not quite first principles, but pretty close).  

Within the **Design Bucket** we have **Specifications** which are requirements that any acceptable design must achieve to be considered 'good enough'.  Specifications also need a justification which references both the design concepts and (of course) this motivation document and associated logic.

- Motivation
  - Motivation
- Design buckets
  - Description
  - Home
  - Food 
  - Community 
- Design Concepts 
  - Description
  - Freedom
  - Purpose 
  - Beauty 
  - Commonality


<details>
  <summary>Notes + bits of text</summary>
  
So, the root cause is -

**Legacy designs and the momentum of production!**

Can you see why religion, revolution, and conspiracy have so many more adherants?

## Die Hard

A **legacy design** is something which may have been well designed a while ago, but is now counterproductive and often hard to change...  Querty keyboards is an example.  The **momentum of production** is the broad idea that things which have achieved a certain scale will, through many forces, persist longer than they probably should.  

We deserve better!  Well... we don't, any more than dogs deserve not to sniff butts.

Deserve or not... technically, we almost certainly can do better.  

I would argue that today, our technological branch  technologically insane branch... but because life is big and full of challenges, we generally can only see one branch up, or one branch back.  We try to do our best, but don't realize we're way out in left field.

No, the next step isn't burn everything down or cut down the tree.

Our technological history contains the pieces to do amazing things - connect people physically and virtually, better understand our universe, and access the core needs of life like food and shelter.

The problem is those pieces aren't arrainged well... they are arrainged around systems and structures which drive themselves.

It's no ones fault.  It's not a conspiracy.  It's a failure to apply first principles to our lives, use the pieces we have, and build something better.

This isn't about peace and love, techno-utopianism, preparing for the end times, escaping the oligarchy...

It's about identifying 
</details>

