Greg notes 

Dead end branch of development of human beings 

Monkey climbing a tree 

Step 1: Blank slate mindset, be willing to start over mentally. If people are bringing current and local needs and wants then won’t work. 


Step 2: human beings were built for something dramatically different and and simpler than this. However you believe things started, we were fundamentally designed to live as part of nature.


At some point we began going down technology decision tree, monkey mock-up a tree making decisions at each branch, and the rate at which we are emoting you now is way faster than ever before. 


Why are we moving this tree? 


Accumulation of power.


No just capitalism, communism and monarchies did the same. Power accumulation drives the decisions that move us down this tree. It’s not about a system but humans of the last 10k years. 


The driver is AOP, but there are consequences to moving down this tree. As the monkey goes along the technology tree, there are also mind body environment trees. Every step we take at the technology tree, you influence the other trees. Previously we worked ourselves to death in coal mines etc and now we have done the opposite hunched over benches. 


Last is the existential threat that we’ve only just started to think about, is the environment. 


Whether you realize it our not, tech tree has consequences and consequences are not part of the equation right now. 


There’s all the things you can do to the human body, and in the last 10k years we’ve taken the narrowness of human experience we were built for and we’ve expanding it dramatically - we have people who sit in chairs all day for 40 years and we have athletes who do nothing but cycle all day. Amazing thing to look at the distribution and see all the places we went. Mind tree, people who lived in caves and people oppressed and people so opulent  can’t understand the e oppressed and people who live in the virtual realm.


Environment tree: Toxic spills in India, killed species, brought them back, paved road and planted grass. 


The diversity of human experience is well beyond what we’ve been built for. 


Let’s stop and think with this blank slate about how we want to design the lives of future generations. We’ve seen all these things, how do we want to design things? 


We can all agree that sitting in front of a computer all day is bad. 

Destructive physical activity to the degree we are dying early, also bad. 

If we were designing life for future generations, we know what is looks like isn’t at the very edges. 

We don’t want to repeat what wasn’t productive in the past. 


Ok then, what do we want to do? 


Do you want to spend 10 hours a day just trying to get food? No.

But what about a system with 1-2 hours a day of physical activity? Yes. 


What is the cost of our efficiency against the value it’s bringing? 


We’ve assumed lower effort and lower cost is best. 


Something in the middle of large scale industrial ag and foraging in the woods. 


The things we create should push us toward the outcomes we intend. 


Now we are so myopic, people find it hard to think about the bigger picture in individual design decisions. 


Engage in this process with this thinking. 

Everybody agreed up to solutions so use that agreement to the talk about solutions. 


>>how to circle back into decentralizing power so we don’t get here again? 


Purpose: frame how we design human systems >> give examples of human systems that exist already


We want to build systems that optimize human health and wellness. 


We don’t want to build systems that make people sit for 40 hours a day. Or that optimize one area so much that it de-optimizes others.  


The systems aren’t set up for people to succeed - creating systems where we are unwell, immense amount of sugar around all things at all times. We are sitting in a system that designed to do that. 


    Consider Audience when crafting examples 
    Give examples of designs that meet these 
    What people are designed to do doesn’t seem to land, workshop a bit! Too vague. Animals are genetically designed for an environment. Polar bear in a jungle. 
    Visual for the 3 trees - everything is connected. Everything has consequences. Use more of an ecosystem or car or something. 
    Accumulation of power part is unclear - need to go over the how we got here? 
    Distribution of human experience is more complex than a distribution or histogram, this part made me pause and feel like it oversimplified peoples lives 
    Give larger number of examples so it just doesn’t feel like it’s about screens and movement
    Start with the why, with questions, get agreement… leading to design criteria 
    Share more of the vision, what it could look like vs what we don’t like now 
    Start with the buy in, ie would you rather work 30 hours a week or 40? Who feels healthy sitting 40 hours a week? Who wants to be healthier? Spend less time on social media? Talk better as a country? 
    We design systems, here’s 
    Clarify timescale : we have not fundamentally evolved, something feels wrong, something could feel a lot better 
    Give people examples of human system design 



Hook: something’s wrong or something could feel a lot better. Think about what doesn’t feel good for you. 

    Why? We are built for something else. Evolution doesn’t happen this fast. We are living now without understanding how intertwined we are with the planet — like a fish not realizing it needs water. And even with our own bodies, virtual vs tangible. Evolution happens over long time scales, biblical or not looked like this. Technology has evolved faster than biology. Rather than evolving genetics we’ve evolved technology. 
    So what? (General): has consequences, let’s not judge it but just observe it. Decision tree— the tech influences our bodies minds and environment. Mental health, how we talk to each other, how we eat, etc.
    Option: How did we get here? Accumulation of power.
    So what? (Specific): Here’s what the looks like. The outcomes of our process so far. The distributions. I’m guessing the concern you thought of in the beginning is captured in one of these graphs. 
    What can we do about it? The good news! We can design for better outcomes! Learn the lessons of the past and be intentional in our technology path. We have the capacity to make choices in how we design things. Give examples of current positive designs. *Imagine you get to design the system not live in it* We already design the systems around us all the time. Examples: Here’s what I’m going to do and what others are doing.
    What can you do in your sphere? Here some high level design rules/criteria 



You can call this permaculture or systems thinking or whatever but whatever you call it this is a push to yes go do that! Widening to broader locations. 


Long-termism 



Add to why or anticipate objections toward the end: 

    Look to the future: here’s what we expect to see. What does the future look like? 


Sent from my iPhone
