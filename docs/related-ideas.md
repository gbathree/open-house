centralization enables technological improvement which relies on scale to progress.
separation 

1. Accumulation of influence
   - youtube influences and youtube as a way to accumulate influence for bad and good
   - russian and oil/gas as another example
   - us with trade as another example
2. Refined sugar
   - taking something which is ok in small amounts in complex systems and refining it and putting it back into the system.
   - literal refined sugar : body
   - social media / information : individuals
   - ...

(This narrative could be all/mostly/a little wrong)

Humans are animals with OCD and a half-developed sense of collaboration.  Our sense of self sits evenly between independent individuals and members on the tribe.

OCD allows humans to develop tools and technology beyond what a normal animal would care about.

Those tools and technologies enable the accumulation of power.  One could argue this is bad.

Accumulation of power allows greater than human sized impacts (physical force, communication of ideas, control of space, etc.) by one or few humans.  Other humans become extensions of that human.

Eventually, groups of powerful humans embed power in the group itself (bureaucracy), a means to have inter-generational power accumulation.  Humans then become cells of the group ‘body’ rather than an extension of other power-seeking humans.

Accumulation of power combines with tools and technology to create specialization, which causes externalization of problems, creating further specialization to solve them.  This is a loop.

Externalization becomes a design pattern – humans default to problem avoidance through seeking further specialization.  In our emotional life (we don’t do the ‘work’ to life with or near others, mental health), in our interaction with the environment (water, air, etc. etc.), in our political life (externalizing responsibility and ethics), in our skills (sports outside of schools, learn only from professionals, etc.).  One could argue that this is inherently bad.

This design pattern is supercharged by technology, because specialization creates complexity which is solvable only by technology.

Externalization causes our sense of self to sit between highly specialized individuals and one or many group ‘bodies’ (rather between individuals and tribes).  This is insufficient for needed human ‘self’ because a) specialization reduced our independence so less ‘self’ comes from within and b) group ‘bodies’ expanded what our sense of self can be defined by and around.  In short, humans can’t do as much, but are expected to do more to achieve this newly-defined ‘whole self’.  This is inherently bad for humans.

OCD, power accumulation and externalization will be further reinforced through evolutionary processes of humans themselves via the long-term levers of evolution: culture, norms, and genetics.  This is also a loop.  One could argue this is bad.

What good came out of this?
- we have ideas/tools/tech to better understand ourselves
- we have ideas/tools/tech to remove the brutal parts of life
- we can have bigger, broader, more diverse senses of self

What bad came out of this?
- we can hurt or even totally destroy our ‘selves’, humanity, and other things (like earth, animals, etc.)

How can we do better?
- prioritize not externalizing… do the work up front to cover your bases
  - Physical Needs: get most food, fuel, and necessary specializations (medical, etc.) met on the smallest reasonable space you can be part of.
  - Emotional Needs: build personal connections and understandings with the people around you before engaging in closer relationships, not after.
- limit the extent of power… Now we know that accumulation of power is a runaway slope with no friction.  Do not externalize the application of that friction.
  - prioritize technologies that solve problems with the least externalization
  - support diversity and movement so diversity achieves outcomes everyone can be happy with.

The greatest unrecognized vice is the constant need for more.  Not only is it unrecognized but it is embedded in our economy as a virtue.  We have pegged the dial to more, and the consequence is that we act more like a virus and less like a being as we become a group.  If we want to become a group, what do we want to become?  The most powerful being that acts like a virus?  
